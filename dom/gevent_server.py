#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gevent.monkey
gevent.monkey.patch_socket()

from server import handle_client, create_socket


def gevent_server_loop(listen_sock):
    try:
        while True:
            client_sock, sockname = listen_sock.accept()
            gevent.spawn(handle_client, client_sock)
    except KeyboardInterrupt:
        listen_sock.close()


if __name__ == '__main__':
    gevent_server_loop(create_socket())