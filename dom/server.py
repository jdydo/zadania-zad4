#!/usr/bin/env python
#  -*- encoding: utf-8 -*-

import socket
from server_http_methods import exam_request, create_500_error_content


#HOST = 'localhost'
HOST = '194.29.175.240'
PORT = 22334


# Metoda tworzy gotowy do użycia socket
def create_socket():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, PORT))
    s.listen(128)
    print 'Serwer ONLINE'
    return s


def handle_client(connection):
    try:
        request = connection.recv(2048)
        if request:
            connection.sendall(exam_request(request))
    except Exception, ex:
        try:
            connection.sendall(create_500_error_content())
        except socket.error:
            pass
    finally:
        connection.close()


def server_loop(s):
    try:
        while True:
            connection, address = s.accept()
            handle_client(connection)
    except KeyboardInterrupt:
        s.close()


if __name__ == '__main__':
    server_loop(create_socket())
