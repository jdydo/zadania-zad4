#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import mimetypes
from server import HOST, PORT
from twisted.web import server, resource
from twisted.internet import reactor
from server_http_methods import exam_content


class ServerResource(resource.Resource):
    isLeaf = True
    numberRequests = 0

    def exam_request(self, request):
        request_method = request.method
        request_proto = request.clientproto
        uri = 'web' + request.uri.rstrip('/')

        if request_proto.find('HTTP') != -1 and request_method.find('GET') != -1 and os.path.exists(uri):
            status = 200
            content, page = exam_content(uri)
        elif request_proto.find('HTTP') != -1 and request_method.find('GET') != -1 and not os.path.exists(uri):
            status = 404
            content = mimetypes.guess_type('error_pages/404.html')[0]
            page = open('error_pages/404.html').read()
        else:
            status = 405
            content = mimetypes.guess_type('error_pages/405.html')[0]
            page = open('error_pages/405.html').read()

        request.setResponseCode(status)
        request.setHeader("Content-Type", content + '; charset=UTF-8')
        return page

    # Metoda tworzy nagłówek dla błędu 500 oraz wczytuje odpowiednią stronę z błędem
    def create_500_error_content(self, request):
        status = 500
        content = mimetypes.guess_type('error_pages/500.html')[0]
        page = open('error_pages/500.html').read()

        request.setResponseCode(status)
        request.setHeader("Content-Type", content + '; charset=UTF-8')
        return page

    def handle_client(self, request):
        try:
            if request:
                response = self.exam_request(request)
        except Exception, ex:
            response = self.create_500_error_content(request)

        return response

    def render_GET(self, request):
        self.numberRequests += 1
        return self.handle_client(request)


if __name__ == '__main__':
    server.address = HOST
    reactor.listenTCP(PORT, server.Site(ServerResource()))
    reactor.run()