#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import unittest
from funkload.FunkLoadTestCase import FunkLoadTestCase
from server import HOST, PORT
from server_test_methods import get_response, get_response_raw


class ServerTestCase(FunkLoadTestCase):
    def setUp(self):
        self.server_url = self.conf_get('main', 'url')

    def test_strony1(self):
        resp = self.get(self.server_url + '/images/jpg_rip.jpg', description='Get JPG')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/lokomotywa.txt', description='Get TXT')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/web_page.html', description='Get HTML')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/', description='Get DIR')
        self.assert_(resp.code in [200], "expecting a 200")

    def test_strony2(self):
        get_response_raw(HOST, PORT, 'GET / HTTP/1.1')

    def test_strony3(self):
        get_response(HOST, PORT, 'GET', '/')


if __name__ in ('main', '__main__'):
    unittest.main()