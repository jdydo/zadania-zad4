# -*- coding: utf-8 -*-

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler
import server
import threading
import signal
import daemon
import lockfile


class ThreadedTCPRequestHandler(BaseRequestHandler):
    def handle(self):
        server.handle_client(self.request)


class ThreadedTCPServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


context = daemon.DaemonContext(
    working_directory='/home/p1',
    pidfile=lockfile.FileLock('/home/p1/server123.pid'),
)

context.signal_map = {
    signal.SIGTERM: 'terminate',
}

with context:
    s = ThreadedTCPServer((server.HOST, server.PORT), ThreadedTCPRequestHandler)
    server_thread = threading.Thread(target=s.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    s.serve_forever()