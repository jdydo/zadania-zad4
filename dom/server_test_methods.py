#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import socket
import httplib


def get_response(host='localhost', port=4444, request_type='GET', page='/'):
    connection = httplib.HTTPConnection(host, port)

    try:
        connection.request(request_type, page)
        response = connection.getresponse()
    except Exception:
        return '', '', ''
    finally:
        connection.close()

    return response.status, response.reason, response.getheaders()


def get_response_raw(host='localhost', port=4444, header=''):
    connection = socket.socket()
    connection.connect((host, port))

    try:
        connection.sendall(header)
        response = connection.recv(2048)
    except socket.error:
        return ''
    finally:
        connection.close()

    return response
