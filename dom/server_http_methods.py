#!/usr/bin/env python
#  -*- encoding: utf-8 -*-

import os
import mimetypes
from email.utils import formatdate


# Metoda dobiera odpowiednie działania w zależności od przetwarzanej zawartości
# (stworzenie listy dostępnych obiektów lub otworzenie zawartości)
def exam_content(uri):
    if os.path.isdir(uri):
        page = '<!DOCTYPE html><html><head><title>Lista plikow</title><meta charset="utf-8"/></head>' \
               '<body><h1>Zawartość katalogu %s :</h1><hr></br><ul>%s</ul></body></html>'

        item_list = ''
        for item in os.listdir(uri):
            item_list += '<li style="margin-bottom: 10px;"><a style="font-size:18px" href="' + uri.lstrip('web')
            if os.path.isdir(uri) and uri != 'web/':
                item_list += '/'
            item_list += item + '">' + item + '</a></li>'

        page = page % (uri.rstrip('/'), item_list)
        content = 'text/html'
    else:
        page = open(uri, 'rb').read()
        content = mimetypes.guess_type(uri)[0]

    return content, page


# Metoda przetwarza żadanie klienta i konstruuje odpowiednią odpowiedź
# oraz parsuje i ewentualnie poprawia uri (nadmiar / lub spacje w adresie)
def exam_request(request):
    request_type = request.split('\r\n')[0]
    uri = 'web' + request.split(' ')[1].rstrip('/')

    if request_type.find('HTTP') != -1 and request_type.find('GET') != -1 and os.path.exists(uri):
        status = 'HTTP/1.1 200 OK'
        content, page = exam_content(uri)
    elif request_type.find('HTTP') != -1 and request_type.find('GET') != -1 and not os.path.exists(uri):
        status = 'HTTP/1.1 404 Not Found'
        content = mimetypes.guess_type('error_pages/404.html')[0]
        page = open('error_pages/404.html').read()
    else:
        status = 'HTTP/1.1 405 Method Not Allowed'
        content = mimetypes.guess_type('error_pages/405.html')[0]
        page = open('error_pages/405.html').read()

    return create_header(status, content, page) + page


# Metoda tworzy nagłówek dla błędu 500 oraz wczytuje odpowiednią stronę z błędem
def create_500_error_content():
    status = 'HTTP/1.1 500 Internal Server Error'
    content = mimetypes.guess_type('error_pages/500.html')[0]
    page = open('error_pages/500.html').read()

    return create_header(status, content, page) + page


# Metoda konstruuje nagłówek na podstawie przesłanyh argumentów
def create_header(status, content, page):
    header = status + '\r\n' \
             + 'Content-Type: ' + content + '; charset=UTF-8' + '\r\n' \
             + 'Content-Length: ' + str(len(page)) + '\r\n' \
             + 'Date: ' + formatdate(timeval=None, localtime=False, usegmt=True) + '\r\n\r\n'

    return header
