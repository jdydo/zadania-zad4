# -*- encoding: utf-8 -*-

import signal
import daemon
import lockfile
from server import run_server


context = daemon.DaemonContext(
    working_directory='/home/jakub/Pulpit/GIT/lab',
    pidfile=lockfile.FileLock('/home/jakub/Pulpit/GIT/lab/server123.pid'),
)

context.signal_map = {
    signal.SIGTERM: 'terminate',
}

with context:
    run_server()