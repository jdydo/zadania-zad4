# -*- encoding: utf-8 -*-

import unittest
from funkload.FunkLoadTestCase import FunkLoadTestCase


class Test(FunkLoadTestCase):
    def setUp(self):
        self.server_url = self.conf_get('main', 'url')

    def test_strony(self):
        resp = self.get(self.server_url + '/images/jpg_rip.jpg', description='Get JPG')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/images/gnu_meditate_levitate.png', description='Get PNG')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/lokomotywa.txt', description='Get TXT')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/web_page.html', description='Get HTML')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url + '/', description='Get DIR')
        self.assert_(resp.code in [200], "expecting a 200")

        resp = self.get(self.server_url+'/dddd', ok_codes=[404], description='Get 404')
        self.assert_(resp.code in [404], "expecting a 404")

        resp = self.post(self.server_url + "/", ok_codes=[405], params=[], description="POST 405")
        self.assert_(resp.code in [405], "expecting a 405")

if __name__ in ('main', '__main__'):
    unittest.main()